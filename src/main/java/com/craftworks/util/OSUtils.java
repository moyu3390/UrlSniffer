package com.craftworks.util;

import oshi.SystemInfo;
import oshi.hardware.NetworkIF;

public final class OSUtils
{
    private OSUtils() {}

    public static NetworkIF[] getUsableNetworkIFs()
    {
        SystemInfo si = new SystemInfo();
        return si.getHardware()
                 .getNetworkIFs(false)
                 .stream()
                 .filter(nif -> nif.getIPv4addr().length > 0)
                 .filter(nif -> nif.getBytesRecv() > 0 && nif.getBytesSent() > 0)
                 .filter(nif -> nif.isConnectorPresent() && nif.getIfOperStatus() == NetworkIF.IfOperStatus.UP)
                 .toArray(NetworkIF[]::new);
    }
}
