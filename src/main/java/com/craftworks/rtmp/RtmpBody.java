package com.craftworks.rtmp;

import com.craftworks.rtmp.amf0.AMF0;
import com.craftworks.rtmp.amf0.DataType;

import java.util.ArrayList;
import java.util.List;

public class RtmpBody
{
    private final List<DataType> dataTypes;
    public RtmpBody(byte[] bytes)
    {
        int offset = 0;
        int end = bytes.length;

        dataTypes = new ArrayList<>();
        while (offset < end)
        {
            int size = AMF0.getDataTypeSize(bytes, offset);
            DataType type = AMF0.decode(bytes, offset);
            if (type != null)
                dataTypes.add(type);
            offset += size;
        }
    }

    public List<DataType> getDataTypeList()
    {
        return dataTypes;
    }
}
