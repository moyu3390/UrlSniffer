package com.craftworks.rtmp.amf0;

import com.craftworks.util.BigEndian;

import java.util.Map;

public class NullType extends DataType
{
    private static final String TYPE_NAME = "NullType";

    public NullType(byte[] bytes, int offset)
    {
        super(0x05, bytes, offset);
    }

    @Override
    protected void decode(byte[] bytes, int offset)
    {
        if (BigEndian.getUINT8(bytes, offset) != type)
            throw new IllegalArgumentException("incorrect bytes encoding");
    }

    @Override
    public String stringValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Double doubleValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Boolean booleanValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Map<String, ObjectProperty> propertyValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public String toString()
    {
        return "NullType{}";
    }
}
