package com.craftworks.rtmp.amf0;

import java.util.Map;

public abstract class DataType
{
    protected int type;
    protected int size;

    protected DataType(int type, byte[] bytes, int offset)
    {
        this.type = type;
        decode(bytes, offset);
    }

    public int getType()
    {
        return type;
    }

    public int getSize()
    {
        return size;
    }

    protected abstract void decode(byte[] bytes, int offset);

    public abstract String stringValue();
    public abstract Double doubleValue();
    public abstract Boolean booleanValue();
    public abstract Map<String, ObjectProperty> propertyValue();
}
