package com.craftworks.rtmp.amf0;

import com.craftworks.util.BigEndian;

import java.util.Map;

public class NumberType extends DataType
{
    private static final String TYPE_NAME = "NumberType";

    private Double value;

    public NumberType(byte[] bytes, int offset)
    {
        super(0x00, bytes, offset);
    }

    @Override
    protected void decode(byte[] bytes, int offset)
    {
        if (BigEndian.getUINT8(bytes, offset) != type)
            throw new IllegalArgumentException("incorrect bytes encoding");

        long longBits = BigEndian.getUINT64(bytes, offset + 1);
        value = Double.longBitsToDouble(longBits);
        size = 9;
    }

    @Override
    public String stringValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Double doubleValue()
    {
        return value;
    }

    @Override
    public Boolean booleanValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Map<String, ObjectProperty> propertyValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public String toString()
    {
        return "NumberType{" +
               "value=" + value +
               '}';
    }
}
