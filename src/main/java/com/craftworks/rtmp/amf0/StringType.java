package com.craftworks.rtmp.amf0;

import com.craftworks.util.BigEndian;

import java.util.Map;

public class StringType extends DataType
{
    private static final String TYPE_NAME = "StringType";

    private String value;

    public StringType(byte[] bytes, int offset)
    {
        super(0x02, bytes, offset);
    }

    @Override
    protected void decode(byte[] bytes, int offset)
    {
        if (BigEndian.getUINT8(bytes, offset) != type)
            throw new IllegalArgumentException("incorrect bytes encoding");

        int nameLen = BigEndian.getUINT16(bytes, offset + 1);
        value = new String(bytes, offset + 3, nameLen);
        size = 3 + nameLen;
    }

    @Override
    public String stringValue()
    {
        return value;
    }

    @Override
    public Double doubleValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Boolean booleanValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Map<String, ObjectProperty> propertyValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public String toString()
    {
        return "StringType{" +
               "value='" + value + '\'' +
               '}';
    }
}
