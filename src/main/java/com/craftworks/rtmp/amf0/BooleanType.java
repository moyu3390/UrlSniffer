package com.craftworks.rtmp.amf0;

import com.craftworks.util.BigEndian;

import java.util.Map;

public class BooleanType extends DataType
{
    private static final String TYPE_NAME = "BooleanType";
    private Boolean value;

    public BooleanType(byte[] bytes, int offset)
    {
        super(0x01, bytes, offset);
    }

    @Override
    protected void decode(byte[] bytes, int offset)
    {
        if (BigEndian.getUINT8(bytes, offset) != type)
            throw new IllegalArgumentException("incorrect bytes encoding");

        value = BigEndian.getUINT8(bytes, offset + 1) == 0 ? Boolean.FALSE : Boolean.TRUE;
        size = 2;
    }

    @Override
    public String stringValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Double doubleValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public Boolean booleanValue()
    {
        return value;
    }

    @Override
    public Map<String, ObjectProperty> propertyValue()
    {
        throw new UnsupportedOperationException(TYPE_NAME);
    }

    @Override
    public String toString()
    {
        return "BooleanType{" +
               "value=" + value +
               '}';
    }
}
