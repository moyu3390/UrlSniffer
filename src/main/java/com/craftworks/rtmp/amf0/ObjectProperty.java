package com.craftworks.rtmp.amf0;

import com.craftworks.util.BigEndian;

public class ObjectProperty
{
    private final String name;
    private final DataType value;
    private final int size;

    public ObjectProperty(byte[] bytes, int offset)
    {
        int nameLen = BigEndian.getUINT16(bytes, offset);
        name = new String(bytes, offset + 2, nameLen);
        offset += 2 + nameLen;
        int type = BigEndian.getUINT8(bytes, offset);
        switch (type)
        {
            case 0x00: // Number type
                value = new NumberType(bytes, offset);
                break;
            case 0x01: // Boolean type
                value = new BooleanType(bytes, offset);
                break;
            case 0x02: // String type
                value = new StringType(bytes, offset);
                break;
            case 0x03: // Object type
                value = new ObjectType(bytes, offset);
                break;
            case 0x05: // Null type
                value = new NullType(bytes, offset);
                break;
            case 0x06: // Undefined type
                value = new UndefinedType(bytes,offset);
                break;
            case 0x0D: // Unsupported type
                value = new UnsupportedType(bytes, offset);
                break;
            default:
                throw new UnsupportedOperationException("unsupported type: " + type);
        }

        size = 2 + nameLen + value.getSize();
    }

    public String getName()
    {
        return name;
    }

    public DataType getValue()
    {
        return value;
    }

    public int getSize()
    {
        return size;
    }

    @Override
    public String toString()
    {
        return "ObjectProperty{" +
               "name='" + name + '\'' +
               ", value=" + value +
               '}';
    }
}
