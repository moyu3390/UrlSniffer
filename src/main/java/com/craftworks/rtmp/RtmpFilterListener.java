package com.craftworks.rtmp;

public interface RtmpFilterListener
{
    void filterStarted();
    void filterStopped();
    void urlFiltered(String url);
    void exceptionCaught(Exception ex);
}
