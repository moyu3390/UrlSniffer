package com.craftworks.rtmp;

import java.util.ArrayList;
import java.util.List;

public final class Rtmp
{
    private static int chunkSize = 128;

    private Rtmp() {}

    public static void setChunkSize(int size)
    {
        chunkSize = size;
    }

    public static int getChunkSize()
    {
        return chunkSize;
    }

    public static List<Chunk> decode(byte[] bytes)
    {
        return decode(bytes, 0, bytes.length);
    }

    public static List<Chunk> decode(byte[] bytes, int offset, int length)
    {
        List<Chunk> chunks = new ArrayList<>();
        while (length > 0)
        {
            Chunk chunk = new Chunk(bytes, offset, length);
            chunks.add(chunk);
            offset += chunk.getSize();
            length -= chunk.getSize();
        }
        return chunks;
    }
 }
