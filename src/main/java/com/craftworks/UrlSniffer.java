package com.craftworks;

import com.craftworks.ui.MainViewController;
import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatLaf;
import lombok.extern.slf4j.Slf4j;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.SingleFrameApplication;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public class UrlSniffer extends SingleFrameApplication
{
    public static final String APP_NAME = "直播助手";
    public static final String APP_VERSION = "0.2.17";
    public static final String APP_VENDOR = "Craftworks";

    private MainViewController controller;

    static void enableAntiAliasing()
    {
        System.setProperty("awt.useSystemAAFontSettings", "on");
        System.setProperty("swing.aatext", "true");
    }

    static void createRecordingFolder()
    {
        try
        {
            Path recordsDir = Paths.get("records");
            Files.createDirectories(recordsDir);
        } catch (IOException ex)
        {
            log.warn("无法创建目录：{}", ex.getMessage());
        }
    }

    public static void main(String[] args)
    {
        createRecordingFolder();
        enableAntiAliasing();
        SingleFrameApplication.launch(UrlSniffer.class, args);
    }

    @Override
    protected void initialize(String[] args)
    {
        FlatDarculaLaf.setup();
//        FlatIntelliJLaf.setup();

        UIManager.put("TabbedPane.showTabSeparators", true);
        UIManager.put("Table.paintOutsideAlternateRows", true);
        UIManager.put("Table.alternateRowColor", FlatLaf.isLafDark() ? new Color(0x626262) : new Color(0xEEEEEE));
    }

    @Override
    protected void startup()
    {
        FrameView frameView = getMainView();
        controller = new MainViewController(frameView);
        show(frameView);
    }

    @Override
    protected void shutdown()
    {
        controller.stopRecordingTask();
        controller.stopFiltering();
        super.shutdown();
    }
}