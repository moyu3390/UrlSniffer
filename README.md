# 直播助手

#### 介绍
抓取直播流地址的工具，可以监看并录制到本地。

#### 软件架构
基于pcap4j，监视本地网卡流量，过滤rtmp消息并提取直播流地址。
取到直播流地址后，再通过ffmpeg工具进行录制。或通过MPC-HC播放器实时观看。

GUI基于Swing Application Framework（bsaf）构建。

#### 安装教程

1.  安装Java 8+环境（JRE、JDK均可）。
2.  安装Maven环境。
3.  下载源代码，进入项目目录，执行mvn clean package，在target目录里找到发布包（url-sniffer-1.0-SNAPSHOT.zip），复制到任意位置，解压。
4.  安装WinPcap或npcap。
5.  双击UrlSniffer.exe启动程序，选择合适的网卡接口，点击“开始监听”。

#### 使用说明

1.  目前仅支持RTMP协议。
2.  如需获取直播APP的地址，需安装安卓模拟器（例如：夜神模拟器）。在模拟器中运行对应的app，然后可通过本程序抓取直播流地址。
3.  请勿用于违法活动。

#### 参与贡献

1.  打赏我
